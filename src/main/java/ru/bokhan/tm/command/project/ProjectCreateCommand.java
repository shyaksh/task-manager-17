package ru.bokhan.tm.command.project;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.util.TerminalUtil;

public class ProjectCreateCommand extends AbstractCommand {

    @Override
    public String name() {
        return "project-create";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Create new project";
    }

    @Override
    public void execute() {
        final String userId = serviceLocator.getAuthService().getUserId();
        System.out.println("[CREATE PROJECT]");
        System.out.println("ENTER NAME:");
        final String name = TerminalUtil.nextLine();
        System.out.println("ENTER DESCRIPTION:");
        final String description = TerminalUtil.nextLine();
        serviceLocator.getProjectService().create(userId, name, description);
        System.out.println("[OK]");
    }

}
