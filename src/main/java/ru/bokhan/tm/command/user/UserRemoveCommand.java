package ru.bokhan.tm.command.user;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.entity.User;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.util.TerminalUtil;

public class UserRemoveCommand extends AbstractCommand {

    @Override
    public String name() {
        return "user-remove";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Remove user by login";
    }

    @Override
    public void execute() {
        System.out.println("[REMOVE USER]");
        System.out.println("ENTER LOGIN:");
        final String login = TerminalUtil.nextLine();
        final User user = serviceLocator.getUserService().removeByLogin(login);
        if (user == null) {
            System.out.println("[FAIL]");
            return;
        }
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
