package ru.bokhan.tm.command.user;

import ru.bokhan.tm.command.AbstractCommand;
import ru.bokhan.tm.enumerated.Role;
import ru.bokhan.tm.util.TerminalUtil;

public class UserLockCommand extends AbstractCommand {
    @Override
    public String name() {
        return "user-lock";
    }

    @Override
    public String argument() {
        return null;
    }

    @Override
    public String description() {
        return "Lock user by login.";
    }

    @Override
    public void execute() {
        System.out.println("[LOCK USER]");
        System.out.println("ENTER LOGIN");
        final String login = TerminalUtil.nextLine();
        serviceLocator.getUserService().lockUserByLogin(login);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.ADMIN};
    }

}
