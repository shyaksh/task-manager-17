package ru.bokhan.tm.api.service;

import ru.bokhan.tm.dto.Domain;

public interface IDomainService {

    void load(Domain domain);

    void export(Domain domain);

}
