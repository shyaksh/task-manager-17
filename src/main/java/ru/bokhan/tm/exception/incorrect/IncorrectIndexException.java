package ru.bokhan.tm.exception.incorrect;

public class IncorrectIndexException extends RuntimeException {

    public IncorrectIndexException() {
        super("Error! Index is incorrect...");
    }

}