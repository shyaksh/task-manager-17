package ru.bokhan.tm.exception.empty;

public class EmptyLastNameException extends RuntimeException {

    public EmptyLastNameException() {
        super("Error! Last Name is empty...");
    }

}